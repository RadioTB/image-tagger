var self = require("sdk/self");
var contextMenu = require("sdk/context-menu");
var {Cc, Cu, Ci} = require("chrome");
Cc["@mozilla.org/net/osfileconstantsservice;1"].
    getService(Ci.nsIOSFileConstantsService).
    init();
const {TextDecoder, TextEncoder, OS} = Cu.import("resource://gre/modules/osfile.jsm", {});
Cu.import("resource://gre/modules/Downloads.jsm");

var downloadDir = OS.Constants.Path.homeDir + "\\Downloads\\";

console.log(downloadDir);


var menuItem = contextMenu.Item(
{
	label: "Save Image With Tags",
	context: contextMenu.SelectorContext("img"),
	contentScriptFile: "./content.js",
	onMessage: function(img){
		var filename = img.imgName;
		var imgData = new Uint8Array(img.imgData);
		OS.File.writeAtomic(downloadDir + filename, imgData);
	}
});	
