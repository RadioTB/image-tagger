﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeBooru
{
    public partial class HomebooruForm : Form
    {
        public HomebooruForm()
        {
            InitializeComponent();
            PopulateTreeView();
            this.treeView1.NodeMouseClick += new TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            this.listView1.ItemActivate += new EventHandler(this.ListView1_ItemActivate);
            this.listView1.ItemSelectionChanged += new ListViewItemSelectionChangedEventHandler(this.ListView1_ItemSelected);
            this.textBox1.KeyDown += new KeyEventHandler(this.textBox_KeyPress);
            this.listView2.ItemActivate += new EventHandler(this.ListView2_ItemActivate);
        }


    }
}
