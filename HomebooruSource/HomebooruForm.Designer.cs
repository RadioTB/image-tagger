﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeBooru
{
    partial class HomebooruForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomebooruForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.listView1 = new System.Windows.Forms.ListView();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.listView2 = new System.Windows.Forms.ListView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addTagsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1159, 628);
            this.splitContainer1.SplitterDistance = 197;
            this.splitContainer1.TabIndex = 0;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(197, 628);
            this.treeView1.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "folder-icon.png");
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.listView1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(958, 628);
            this.splitContainer2.SplitterDistance = 763;
            this.splitContainer2.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.LargeImageList = this.imageList2;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(763, 628);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList2.ImageSize = new System.Drawing.Size(150, 150);
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.button1);
            this.splitContainer3.Panel1.Controls.Add(this.textBox1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.listView2);
            this.splitContainer3.Size = new System.Drawing.Size(191, 628);
            this.splitContainer3.SplitterDistance = 71;
            this.splitContainer3.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(125, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(61, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(4, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(115, 20);
            this.textBox1.TabIndex = 0;
            // 
            // listView2
            // 
            this.listView2.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.Location = new System.Drawing.Point(0, 0);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(191, 553);
            this.listView2.TabIndex = 0;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.List;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTagsToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 26);
            // 
            // addTagsToolStripMenuItem
            // 
            this.addTagsToolStripMenuItem.Name = "addTagsToolStripMenuItem";
            this.addTagsToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.addTagsToolStripMenuItem.Text = "Add Tags";
            // 
            // HomebooruForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 628);
            this.Controls.Add(this.splitContainer1);
            this.Name = "HomebooruForm";
            this.Text = "Homebooru";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem addTagsToolStripMenuItem;
        private int imageInd;
        private string currentDirectory;

        #region TreeView Populate
        private void PopulateTreeView()
        {
            TreeNode rootNode;

            DirectoryInfo info = new DirectoryInfo(System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile));
            if (info.Exists)
            {
                rootNode = new TreeNode(info.Name);
                rootNode.Tag = info;
                GetDirectories(info.GetDirectories(), rootNode);
                treeView1.Nodes.Add(rootNode);
            }
        }

        private void GetDirectories(DirectoryInfo[] subDirs, TreeNode nodeToAddTo)
        {
            TreeNode aNode;
            DirectoryInfo[] subSubDirs;
            foreach (DirectoryInfo subDir in subDirs)
            {
                aNode = new TreeNode(subDir.Name, 0, 0);
                aNode.Tag = subDir;
                aNode.ImageKey = "folder";
                try
                {
                    subSubDirs = subDir.GetDirectories();
                    if (subSubDirs.Length != 0)
                    {
                        GetDirectories(subSubDirs, aNode);
                    }
                    nodeToAddTo.Nodes.Add(aNode);
                
                }
                catch(Exception e)
                {

                }
            }
        }
        #endregion


        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode newSelected = e.Node;
            listView1.Items.Clear();
            DirectoryInfo nodeDirInfo = (DirectoryInfo)newSelected.Tag;
            currentDirectory = nodeDirInfo.FullName;
            listView1Populate(nodeDirInfo, string.Empty);
            
        }

        private void listView1Populate(DirectoryInfo dir, string filters)
        {
            this.Cursor = Cursors.WaitCursor;
            listView1.Clear();
            imageInd = 0;
            imageList2.Images.Clear();
            ListViewItem item = null;
            foreach (FileInfo file in dir.GetFiles())
            {

                int[] magicNumbers = new int[9];
                try
                {
                    FileStream fs = file.OpenRead();


                    int count = 0;
                    while (count < 8)
                    {
                        magicNumbers[count] = fs.ReadByte();
                        count++;
                    }

                    if (file.Length > 0)
                    {
                        fs.Seek(file.Length - 1, 0);
                        magicNumbers[8] = fs.ReadByte();

                    }

                    fs.Close();
                    fs.Dispose();
                }
                catch(Exception e)
                {

                }



                if (isImage(magicNumbers))
                {
                    populateImageList2(file.Name);
                    item = new ListViewItem(file.Name, imageInd++);
                    if (string.IsNullOrEmpty(filters))
                    {
                        listView1.Items.Add(item);
                    }
                    else
                    {
                        string[] tags = getTags(file);
                        string[] fil = filters.Split(null);
                        bool allFilters = true;
                        string strTag = "";
                        foreach (string tag in tags)
                        {
                            strTag = strTag + tag;
                        }
                        foreach(string f in fil)
                        {
                            if (strTag.Contains(f)) allFilters = allFilters && true;
                            else allFilters = allFilters && false;
                        }
                        if(allFilters) listView1.Items.Add(item);
                    }
                }

            }


            this.Cursor = Cursors.Default;
        }

        private void populateImageList2(string name)
        {

                DirectoryInfo di = new DirectoryInfo(System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile) + @"\Pictures\thumbs");
                if (!di.Exists) di.Create();
                FileInfo thumbFile = new FileInfo(di.FullName + @"\" + name);
                if(thumbFile.Exists)
                {
                    Image thumb = Image.FromFile(thumbFile.FullName);
                    imageList2.Images.Add(thumb);
                    thumb.Dispose();


                }
                else
                {
                    Image original = Image.FromFile(currentDirectory +@"\" + name);
                    Image thumb = FixedSize(original, 150, 150);
                    FileStream fs = new FileStream(thumbFile.FullName, FileMode.CreateNew);
                    ImageConverter ic = new ImageConverter();
                    byte[] thumbBytes = (byte[])ic.ConvertTo(thumb, typeof(byte[]));
                    fs.Write(thumbBytes, 0, thumbBytes.Length);
                    fs.Close();
                    imageList2.Images.Add(thumb);
                    original.Dispose();
                    thumb.Dispose();
                }
        }

        private bool isImage(int[] bytes)
        {
            int[] png = {137, 80, 78, 71, 13, 10, 26, 10};
            int[] jpg = {255, 216};
            int[] gif = {71, 73, 70, 56};
            for(int i = 0; i < 8; i++)
            {
                if (bytes[i] != png[i]) break;

                if(i == 7)return true;
            }

            for (int i = 0; i < 2; i++)
            {
                if (bytes[i] != jpg[i]) break;

                if(i == 1) return true;
            }

            for (int i = 0; i < 4; i++)
            {
                if (bytes[i] != gif[i]) break;

                if (i == 1) return true;
            }

            return false;
        }

        private Image FixedSize(Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width -
                              (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height -
                              (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height,
                              PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                             imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(System.Drawing.Color.White);
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        private void ListView1_ItemActivate(object sender, EventArgs e)
        {
                  
            string selectedFile = listView1.SelectedItems[0].Text;

            // If it's a file open it
            if (File.Exists(Path.Combine(currentDirectory, selectedFile)))
            {
                //MessageBox.Show(currentDir + @"\" + selectedFile);
                try
                {
                    System.Diagnostics.Process.Start(Path.Combine(currentDirectory, selectedFile));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.StackTrace);
                }
            }
        }

        private void ListView1_ItemSelected(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                string selectedFile = listView1.SelectedItems[0].Text;
                if (File.Exists(Path.Combine(currentDirectory, selectedFile)))
                {
                    FileInfo info = new FileInfo(Path.Combine(currentDirectory, selectedFile));
                    listView2Populate(info);
                }
            }
        }

        private void listView2Populate(FileInfo fi)
        {
            listView2.Items.Clear();

            string[] list = getTags(fi);

            for (int i = 0; i < list.Length; i++)
            {
                ListViewItem item2 = new ListViewItem(list[i]);
                listView2.Items.Add(item2);
            }

        }

        private string[] getTags(FileInfo fi)
        {
            string tags = string.Empty;
            FileStream fs = new FileStream(fi.FullName, FileMode.Open);
            fs.Seek(fi.Length - 1, 0);
            int end = fs.ReadByte();
            if (end == 31)
            {
                char charAt = ' ';
                while (charAt != 31)
                {
                    fs.Seek(-2, SeekOrigin.Current);
                    charAt = (char)fs.ReadByte();

                    tags = charAt + tags;
                }
            }

            fs.Close();

            string[] list = tags.Split(null);

            return list;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DirectoryInfo fi = new DirectoryInfo(currentDirectory);
            listView1Populate(fi, textBox1.Text);
        }

        private void textBox_KeyPress(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(this, new EventArgs());
            }
        }

        private void ListView2_ItemActivate(object sender, EventArgs e)
        {
            textBox1.Clear();
            ListViewItem lvi = listView2.SelectedItems[0];
            textBox1.Text = lvi.Text;
            button1_Click(this, new EventArgs());
        }

    }
}

